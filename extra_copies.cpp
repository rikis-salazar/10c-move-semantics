#include <iostream>

const int SIZE = 3;

// A simple thingy that needs to implement the big 4.
struct Cosa{
    int* data;

    Cosa() : data( new int[SIZE] ) {
        std::cout << "Default constructor...\n";
    }

    Cosa(const Cosa& b ) : data( new int[SIZE] ) {
        std::cout << "COPY constructor...\n";
        for ( size_t i = 0 ; i < SIZE ; ++i ){
            data[i] = b.data[i];
        }
    }

    Cosa& operator=( Cosa rhsClone ){
        std::cout << "Assignment operator\n";
        std::swap(data,rhsClone.data);
        return *this;
    }

    ~Cosa() { 
        std::cout << "Destructor...\n";
        delete[] data; 
    }

};


// A simple function that returns by value
Cosa duplicateCosa( const Cosa& x ){
    Cosa newCosa;
    for ( int i = 0 ; i < SIZE ; i++ )
        newCosa.data[i] = x.data[i];

    return newCosa;
}



// A simple driver...
int main(){
    Cosa a;
    Cosa b(a);
    Cosa c = a;
    b = a;

    std::cout << "\tExtra copies?!...\n";
    Cosa d = Cosa() ;
    d = d;

    std::cout << "\tExtra copies?!...\n";
    a = duplicateCosa(a);


    std::cout << "One or more Cosas?\n";
    Cosa z = Cosa( Cosa( Cosa(a) ) );


    return 0;
}



/** 
  
    O   U   T   P   U   T

    When compiled with 
    g++ (GCC) 5.4.0

    with the extra flags:

        -ansi sets the standards to c++98
        -fno...ors does not provide optimization for returns by value

    The output is shown below

        -ansi (no move semantics, both columns)
        -fno-elide-constructors (right column)


Default constructor...						Default constructor...
COPY constructor...						COPY constructor...
COPY constructor...						COPY constructor...
COPY constructor...						COPY constructor...
Assignment operator						Assignment operator
Destructor...							Destructor...
	Extra copies?!...						Extra copies?!...
Default constructor...						Default constructor...
COPY constructor...						COPY constructor...
							      >	Destructor...
							      >	COPY constructor...
Assignment operator						Assignment operator
Destructor...							Destructor...
	Extra copies?!...						Extra copies?!...
Default constructor...						Default constructor...
							      >	COPY constructor...
							      >	Destructor...
							      >	COPY constructor...
Assignment operator						Assignment operator
Destructor...							Destructor...
							      >	Destructor...
One or more Cosas?						One or more Cosas?
COPY constructor...						COPY constructor...
							      >	COPY constructor...
							      >	COPY constructor...
							      >	COPY constructor...
							      >	Destructor...
							      >	Destructor...
							      >	Destructor...
Destructor...							Destructor...
Destructor...							Destructor...
Destructor...							Destructor...
Destructor...							Destructor...
Destructor...							Destructor...

*/
