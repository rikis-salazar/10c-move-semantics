// l-vlues [locator-values] in C++ are... well they are not 
// c-style l-values (expressions that can appear on the left side
// of = [ assignment operator ]

#include <iostream>
#include <string>

using std::cout;
using std::string;

const int YEAR = 2016;


int global_x = 4;


int& get_ref_int(){
    return global_x;
}


/** 
    No can do!
    error: binding ‘const int’ to reference of type ‘int&’ discards qualifiers
 
int& year_by_ref(){
    return YEAR;
}

*/


int main(){

    int a = 8;
    const int y = 3;

    // The following are l-values
    cout << "a= " << a << ", &a= " << &a <<"\n";
    cout << "y= " << y << ", &y= " << &y <<"\n";
    cout << "global_x= " << get_ref_int() 
         << ", &global_x= " << &get_ref_int() << "\n";

    /** NO CAN DO. See above!
    cout << "year_by_ref()= " << year_by_ref() 
         << ", &year_by_ref()= " << &year_by_ref() << "\n";

    */

    // Some l-values CAN appear on the left side of an assignment =
    a = 9;
    get_ref_int() = 2016;

    cout << "\nAfter changes...\n";
    cout << "a= " << a << ", &a= " << &a <<"\n";
    cout << "global_x()= " << get_ref_int() 
         << ", &global_x()= " << &get_ref_int() << "\n";


    /**
        However, some l-values CANNOT appear on the left side of = 
        error: assignment of read-only variable ‘y’

    y = 0;
     */
      


    // SUMMARY!
    // Unlike in C, in C++ the term l-value does not mean that 
    // an expression can appear in the left side of = [ assignment ].
    // Instead, in C++ l-values are things that can be located in
    // memory (e.g., via a memory address).
    //
    // Also, l-value references [ i.e., regular references ] cannot
    // bind to `const` objects.:

    return 0;
}
