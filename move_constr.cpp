// Implementation of custom MOVE operators.

#include <iostream>

const int SIZE = 3;

// A simple thingy that needs to implement the big 4.
struct Cosa{
    int* data;

    Cosa() : data( new int[SIZE] ) {
        std::cout << "Default constructor...\n";
    }

    Cosa(const Cosa& b ) : data( new int[SIZE] ) {
        std::cout << "COPY constructor...\n";
        for ( size_t i = 0 ; i < SIZE ; ++i ){
            data[i] = b.data[i];
        }
    }

    Cosa& operator=( const Cosa& rhs ){
        std::cout << "Assignment operator\n";
        Cosa rhsClone(rhs);
        std::swap(data,rhsClone.data);
        return *this;
    }

    ~Cosa() { 
        std::cout << "Destructor...\n";
        delete[] data; 
    }

    // There are two other operators
    // MOVE CONSTRUCTOR
    Cosa( Cosa&& b ) : data( new int[SIZE] ) {
        std::cout << "MOVE constructor: Stealing resources...\n";
        data = b.data;
        b.data = nullptr;
        // std::swap( data, b.data ); // <-- Bad idea! Why?
    }

    // MOVE ASSIGNMENT
    // Not really needed... Why?
    Cosa& operator=( const Cosa&& rhs ){
        std::cout << "MOVE assignment...\n";
        /**
        Cosa rhsClone(rhs);
        std::swap(data,rhsClone.data);
        return *this;
        */
        return *this = rhs ;
    }
    
};


// A simple function that returns by value
// Cosa duplicateCosa( const Cosa& x ){
Cosa duplicateCosa( Cosa x ){
    Cosa newCosa;
    for ( int i = 0 ; i < SIZE ; i++ )
        newCosa.data[i] = x.data[i];

    return newCosa;
}



// A simple driver...
int main(){
    Cosa a;
    std::cout << "\n";

    Cosa b( [](){ return Cosa(); }() );
    std::cout << "\n";

    b = a;
    std::cout << "\n";

    std::cout << "\tExtra copies?!...\n";
    Cosa d( duplicateCosa(a) ) ;
    std::cout << "\n";

    std::cout << "\tExtra copies?!...\n";
    a = duplicateCosa(a);
    std::cout << "\n";

    std::cout << "One or more Cosas?\n";
    Cosa z = Cosa( Cosa( Cosa(a) ) );

    return 0;
}



/** 
  
    O   U   T   P   U   T

    When compiled with 
    g++ (GCC) 5.4.0

    with the extra flags: -std=c++11 -fno-elide-constructors

    -std=c++11 sets the standards to c++11 (both columns)
    -fno...ors does not provide optimization for returns by value (right column)


Default constructor...						Default constructor...

Default constructor...						Default constructor...
							      >	MOVE constructor: Stealing resources...
							      >	Destructor...
							      >	MOVE constructor: Stealing resources...
							      >	Destructor...

Assignment operator						Assignment operator
COPY constructor...						COPY constructor...
Destructor...							Destructor...

	Extra copies?!...						Extra copies?!...
COPY constructor...						COPY constructor...
Default constructor...						Default constructor...
							      >	MOVE constructor: Stealing resources...
							      >	Destructor...
							      >	MOVE constructor: Stealing resources...
							      >	Destructor...
Destructor...							Destructor...

	Extra copies?!...						Extra copies?!...
COPY constructor...						COPY constructor...
Default constructor...						Default constructor...
							      >	MOVE constructor: Stealing resources...
							      >	Destructor...
MOVE assignment...						MOVE assignment...
Assignment operator						Assignment operator
COPY constructor...						COPY constructor...
Destructor...							Destructor...
Destructor...							Destructor...
Destructor...							Destructor...

One or more Cosas?						One or more Cosas?
COPY constructor...						COPY constructor...
							      >	MOVE constructor: Stealing resources...
							      >	MOVE constructor: Stealing resources...
							      >	MOVE constructor: Stealing resources...
							      >	Destructor...
							      >	Destructor...
							      >	Destructor...
Destructor...							Destructor...
Destructor...							Destructor...
Destructor...							Destructor...
Destructor...							Destructor...

*/
