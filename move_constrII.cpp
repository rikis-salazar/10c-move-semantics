// WARNING: I finished this example during class. As such it might not even
// compile!!!
//
// Implementation of custom MOVE operators. Part II.  Extra fields.

#include <iostream>
#include <string>

const int SIZE = 3;


// Some Cosas come with extra 'inner stuff'
struct InnerStuff{
    std::string name;   // an object
    bool isUsed;        // a primitive type

    InnerStuff( std::string s = "Some cosa", bool b = true )
      : name(s), isUsed(b) {}

    // Copy constr.
    InnerStuff( const InnerStuff& b ) 
      : name( b.name ), isUsed( b.isUsed ) {} 
    
    // Move constr.
    InnerStuff( InnerStuff&& b ) 
      : name( std::move(b.name) ), isUsed( std::move(b.isUsed) ) {} 
};



// A simple thingy that needs to implement the big 4.
struct Cosa{
    int* data;
    InnerStuff other_stuff;

    Cosa() : data( new int[SIZE] ), other_stuff( InnerStuff() ) {
        std::cout << "Default constructor...\n";
    }

    Cosa(const Cosa& b ) : data( new int[SIZE] ), other_stuff( b.other_stuff ) {
        std::cout << "COPY constructor...\n";
        for ( size_t i = 0 ; i < SIZE ; ++i ){
            data[i] = b.data[i];
        }
    }

    Cosa& operator=( const Cosa& rhs ){
        std::cout << "Assignment operator\n";
        Cosa rhsClone(rhs);
        std::swap(data,rhsClone.data);
        return *this;
    }

    ~Cosa() { 
        std::cout << "Destructor...\n";
        delete[] data; 
    }

    // There are two other operators
    // MOVE CONSTRUCTOR
    Cosa( Cosa&& b ) 
      : data( new int[SIZE] ), other_stuff( std::move(b.other_stuff) ) {
        std::cout << "MOVE constructor: Stealing resources...\n";
        data = b.data;
        b.data = nullptr;
        // std::swap( data, b.data ); // <-- Bad idea! Why?
    }
    
};


// A simple function that returns by value
// Cosa duplicateCosa( const Cosa& x ){
Cosa duplicateCosa( Cosa x ){
    Cosa newCosa;
    for ( int i = 0 ; i < SIZE ; i++ )
        newCosa.data[i] = x.data[i];

    return newCosa;
}



// A simple driver...
int main(){
    Cosa a;
    std::cout << "\n";

    Cosa b( [](){ return Cosa(); }() );
    std::cout << "\n";

    b = a;
    std::cout << "\n";

    std::cout << "\tExtra copies?!...\n";
    Cosa d( duplicateCosa(a) ) ;
    std::cout << "\n";

    std::cout << "\tExtra copies?!...\n";
    a = duplicateCosa(a);
    std::cout << "\n";

    std::cout << "One or more Cosas?\n";
    Cosa z = Cosa( Cosa( Cosa(a) ) );

    return 0;
}



/** 
  
    O   U   T   P   U   T

    When compiled with 
    g++ (GCC) 5.4.0

    with the extra flags: -std=c++11 -fno-elide-constructors

    -std=c++11 sets the standards to c++11
    -fno...ors does not provide optimization for returns by value


Default constructor...

Default constructor...
MOVE constructor: Stealing resources...
Destructor...
MOVE constructor: Stealing resources...
Destructor...

Assignment operator
COPY constructor...
Destructor...

	Extra copies?!...
COPY constructor...
Default constructor...
MOVE constructor: Stealing resources...
Destructor...
MOVE constructor: Stealing resources...
Destructor...
Destructor...

	Extra copies?!...
COPY constructor...
Default constructor...
MOVE constructor: Stealing resources...
Destructor...
MOVE assignment...
Assignment operator
COPY constructor...
Destructor...
Destructor...
Destructor...

Destructor...
Destructor...
Destructor...

*/
