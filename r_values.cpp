// r-values in C++ are... well, they ARE NOT locator-values.

#include <iostream>
#include <string>

using std::cout;
using std::string;


const string YEAR = "Dos mil dieciseis"; // <-- "XXX" is not a string...
int global_x = 4;


// An expression is an r-value if it results in a temporary object
int get_val_int(){
    return global_x;
}

string get_val_year(){
    return YEAR;
}



int main(){

    int a = 8;
    const int y = 3;

    // The following is not an r-values
    cout << "global_x= " << global_x 
         << ", &global_x= " << &global_x <<"\n";

    // However this IS an r-value
    cout << "global_x= " << get_val_int() 
        /**
         << ", &global_x= " << &get_val_int() 

         error: lvalue required as unary ‘&’ operand
         */
         << "\n";



    // Another example of an r-value
    cout << "This year is " << get_val_year() 
         << ", &get_val_year= " << &get_val_year() 
        /**

         error: taking address of temporary [-fpermissive]
         */
         << "\n";

    // NOTE(S): 
    //   + The compiler does not complain about get_val_year()
    //     not being an l-value. Which might hint that it actually
    //     is an l-value.
    //   + &get_val_year() actually DOES return an address when
    //     the flag -fpermissive (GCC 5.4.0) is used.


    /**
        To answer the question of whether or not things are 
        r-values we could try the old c test:

    2016 = 2016 + 1;  // 2016++; ???
    get_val_int() = 2016;
    error: lvalue required ...

    get_val_year() = "Hola, mundo!"; // WTH???!!!

      
     */
    // get_val_year() = "Hola, mundo!";
    // cout << "This year is " << get_val_year() << "\n";


    // SUMMARY!
    // 
    // Some temporaries are actually assigned valid memory addresses.
    // This is cool because it might allows us to `steal` resources
    // to efficiently copy objects.
    // 
    // Also, things get interesting now... turns out some objects could
    // be considered as both l-values and r-values. Out goal here 
    // is not to understand these differences, but if you are curious
    // Check out prof. DeSalvo's slides (this repo).

    return 0;
}
