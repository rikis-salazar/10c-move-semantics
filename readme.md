# `rvalue` references and Move Semantics

In this document we study the ideas behind **Move Semantics** and how is it
that it helps speed up a program by allowing the transfer of resources without
the need to perform deep copies. 

## Return by value is inefficient

Let us consider the following program

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#include <iostream>

const int SIZE = 3;

// Simple thing that needs to implement the big 4.
struct Cosa{
    int* data;

    Cosa() : data( new int[SIZE] ) {}

    Cosa( const Cosa& b ) : data( new int[SIZE] ) {
        for ( size_t i = 0 ; i < SIZE ; ++i ){
            data[i] = b.data[i];
        }
    }

    Cosa& operator=( Cosa rhsClone ){
        std::swap(data,rhsClone.data);
        return *this;
    }

    ~Cosa() { delete[] data; }
};


// Return by value function
Cosa duplicateCosa( const Cosa& x ){
    Cosa newCosa;
    for ( int i = 0 ; i < SIZE ; i++ )
        newCosa.data[i] = x.data[i];

    return newCosa;
}


int main(){

    Cosa a;
    a = duplicateCosa(a);

    return 0;
}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If we run this program as is, depending on how aggressive the compiler
decides to act, the extra copies made in `duplicateCosa` as well as the 
assignment operator might not actually be performed. However, under the
strict meaning of return by value, we see that this code is extremely
inefficient. We'll see by the end of this document, that under C++11 
standards, this is no longer the case.


## `l-values` vs `r-values` [ vs _other_-values ]

Before `C` there existed `CPL`. This language was one of the first to 
introduce value categories for expressions:

> ... all CPL expressions can be evaluated in "right-hand mode", but 
> only certain kinds of expression are meaningful in "left-hand mode".

The `C` programming language borrowed heavily form this taxonomy, and
you will often hear [read] that the term `l-value` stands for _left value_.
In other words, something is an _l-value_ if it can appear on the left
hand side of an assignment operator. While this is **mostly true**, in 
`C++` things are not quite that simple, unfortunately.

The term **l-value** stands for **locator value**. To [try to] understand
the difference, consider the following:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
int global_x = 4;
...
int& get_ref_int(){
    return global_x;
}
... 
int main(){
    int a = 8;
    const int y = 3;

    // Not surprisingly: `global_x`, and `a`, are locator-values.
    // But [perhaps] surprisignly: `y`, and `get_ref_int()`, are 
    // l-values as well. If you want to 'locate' any of the above 
    // expressions use the ampersand operator.
    // E.g. cout << &get_ref_int() ;
    ...

    // Some locator values CAN appear on the left hand side of =
    a = 9;
    get_ref_int() = 2016;

    // While some others CANNOT
    y = 0 ;  // <-- NO CAN DO! `y` is a const variable.
    ... 
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

On the other hand, the definition of the term **rvalue** can get a little
messy... For example, temporary objects are considered to be rvalues. However,
one might argue you that _they can be located_ [e.g. one can display the
memory address of a temporary object by using the compiler flag `-fpermissive`
in `g++ (GCC)`].


## Detecting `rvalues`

In our first example, the issue with extra copies results from the use of
a temporary object, namely `newCosa` in the `duplicateCosa()` function.
Imagine if there was a _magic_ way to tell whether or not an object is
a temporary one. Why, you may ask? Because in this case, since the object
will disappear shortly, we can in principle **steal resources** from it.
The object wouldn't care. And most importantly, stealing results in a more
efficient way to _transfer_ those resources.

> Warning:  
> One must be careful here. When _stealing_, it is important
> to leave the temporary objects in a safe state. In principle, they will
> disappear shortly. We do not want them to crash our programs when they
> go out of scope.

To make a long story short. In `C++11` **rvalue** references were introduced
to identify objects that **can be moved** (i.e., do not mind if they lose
their resources). However, be aware that this is independent of whether or
not these objects can only appear on the right hand side of the assignment
operator. Objects that can only appear on the right hand side of `operator=`
are known as **prvalues** [pure rvalues].

To let the compiler know that it should expect an rvalue reference, an extra
ampersand is needed. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
type& nameOfRegularRef;   // <-- regular reference
type&& nameOfRvalueRef;   // <-- rvalue reference
//  ^^ Not to be confused with a 'reference to a reference'.
//     Those do not exist in C++
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## Move constructor and move assignment

We know we should steal in order to be more efficient. The question is now:
_How do we do so?_ The answer is actually quite simple: first, let the compiler
do the hard work of detecting movable objects; then proceed to do the stealing.

For example, assuming we have a `Cosa` object that handles a dynamic array of 
integers `int* data`; first we use the signature `Cosa( Cosa&& other )` to
declare a move constructor, then we proceed to steal the array from `other`.
Finally, we make sure `other` is left in a valid state before we let it die.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Cosa{
    private:
        int* data;
        ...

    public:
        ...
        // Move constructor
        Cosa ( Cosa&& other ){      // What? No const???
            data = other.data;      // <-- Finders keepers.  3:-)
            other.data = nullptr;   // <-- Losers weepers.
        }
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

> Question: What would happen if we remove the statement `other.data = nullptr;`
> from the move constructor above?  
>
> _HINTS:_  
> 1) both `data`'s point to the same memory address, and  
> 2) `other` is very likely a temporary object.

Fair enough, isn't it? Better yet, it turns out that this simple function
also provides a **move assignment**. How so? Let us look at a typical 
implementation of `operator=`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Cosa{
    public:
        // Copy & Swap assignment operator
        Cosa& operator=( Cosa rhsClone ){    // <-- Two ways to 'copy'! 
            // Fields are safely swapped here
            return *this;
        }
        ...
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

If the compiler detects an rvalue in the right hand side of the
assignment operator, it calls the move constructor instead if the copy
constructor. On the other hand, if an rvalue is not detected, the
regular copy constructor is used. Neat, right?


## To move or not to move

Let's face it, _move semantics_ seems to be a pretty cool idea. However, 
the devil is in the details! One is likely to mess things up if the concepts
are not properly understood. Fortunately, in 98.814% of the time you will
spend programming/studying in C++, you will not need to worry about this.
Turns out that in very few cases (~1% of the time), the compiler will 
not provide these move operators for you. Moreover, even if you could
extract a few precious microseconds out of your program by providing
custom move operators, chances are the compiler will likely ignore your
custom functions (e.g., when returning by value) and will perform its
own optimizations.

> OK, but what about the remaining 0.186% of the time mentioned above? 
> 
> Hint: Just make sure you get the general idea behind move semantics
> before attempting to answer C++ exam/interview questions.
>
